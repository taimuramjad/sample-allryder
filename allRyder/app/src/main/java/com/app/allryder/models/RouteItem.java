package com.app.allryder.models;

import java.util.ArrayList;

/**
 * Created by hamid on 5/10/15.
 */
public class RouteItem {

    public String type;
    public String provider;
    public Properties properties;
    public Price price;
    public ArrayList<Segment> segments;

    public static class Segment{
        public String name;
        public Integer num_stops;
        public String travel_mode;
        public String description;
        public String color;
        public String icon_url;
        public String polyline;
        public ArrayList<Stop> stops;
    }

    public static class Stop{
        public double  lat;
        public double  lng;
        public String datetime;
        public String name;
        public String properties;
    }

    public static class Price{
        public double  amount;
        public String currency;
    }

    public static class Properties{
        public String address;
        public String model;
        public String license_plate;
        public Integer fuel_level;
        public String engine_type;
        public String internal_cleanliness;
        public String description;
        public Integer seats;
        public Integer doors;
    }
}
