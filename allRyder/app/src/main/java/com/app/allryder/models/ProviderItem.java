package com.app.allryder.models;

/**
 * Created by hamid on 5/10/15.
 */
public class ProviderItem {

    public String provider_icon_url;
    public String disclaimer;
    public String ios_itunes_url;
    public String ios_app_url;
    public String android_package_name;
    public String display_name;
}
