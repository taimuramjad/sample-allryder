package com.app.allryder.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import com.app.allryder.R;

/**
 * Launcher Activity of application.
 */
public class StartupActivity extends Activity {

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);

        /*
        set activity background null to remove overdraw
         */
        getWindow().setBackgroundDrawable(null);

        /*
        timer to show splash screen
         */
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                /*
                 This method will be executed once the timer is over
                 Start HomeActivity
                 */
                Intent i = new Intent(StartupActivity.this, HomeActivity.class);
                startActivity(i);
                /*
                 close this activity
                 */
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
