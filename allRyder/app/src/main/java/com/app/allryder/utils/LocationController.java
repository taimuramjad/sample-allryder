package com.app.allryder.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Gives Lat Lng location of Phone and send location to server
 *
 * @see android.Manifest - Permission "INTERNET", "ACCESS_NETWORK_STATE" and
 *      "ACCESS_FINE_LOCATION"
 */
public class LocationController {

	protected Context context;
	public boolean retVal = false;
	public static int DEFAULT_RADIUS = 0;
	// The maximum distance the user should travel between location updates.
	public static int MIN_DISTANCE = DEFAULT_RADIUS / 2;
	// The maximum time that should pass before the user gets a location update.
	public static long MIN_TIME = 3 * 60 * 1000;
	private static volatile LocationController instance;

	int parts = 0;
	
	private LocationController(Context ctx) {
		this.context = ctx;
	}

	/**
	 * 
	 * @param ctx - Context
	 * @return LatLng - contains lat, lng
	 */
	// ///////////////////////////////////////////////////////////////////////////////////
	public LatLng getCurrentLocation(Context ctx) {
		if (context == null)
			return null;
		Location bestResult = null;
		LegacyLastLocationFinder lastLocationFinder = new LegacyLastLocationFinder(
				context);
		lastLocationFinder
				.setChangedLocationListener(oneShotLastLocationUpdateListener);
		bestResult = lastLocationFinder.getLastBestLocation(MIN_DISTANCE,
				MIN_TIME);
		if(bestResult == null){
            return new LatLng(0,0);
        }
        else{
            return new LatLng(bestResult.getLatitude(), bestResult.getLongitude());
        }
	}

	/**
	 * LocationController static instance (Singleton model)
	 * 
	 * @param ctx - Context
	 * @return LocationController - instance
	 */
	// ///////////////////////////////////////////////////////////////////////////////////
	public static LocationController getInstance(Context ctx) {
		if (instance == null) {
			synchronized (LocationController.class) {
				if (instance == null) {
					instance = new LocationController(ctx);
				}
			}
		}
		return instance;
	}

	/**
	 * check Internet connection
	 * 
	 * @param context
	 *            - Context
	 * @return boolean - true; if internet available else false
	 */
	// ///////////////////////////////////////////////////////////////////////////////////
	public boolean isConnectedToInternet(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo bestCon = connManager.getActiveNetworkInfo();

		if (bestCon != null) {
			if (bestCon.isConnected())
				return true;
		}
		return false;
	}

	/*
	 * Location listener
	 */
	// ///////////////////////////////////////////////////////////////////////////////////
	protected LocationListener oneShotLastLocationUpdateListener = new LocationListener() {
		public void onLocationChanged(Location l) {
		}

		public void onProviderDisabled(String provider) {
		}

		public void onStatusChanged(String provider, int status, Bundle extras) {
		}

		public void onProviderEnabled(String provider) {
		}
	};


    public String getAddressFromLocation(Context context, LatLng point){
        Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
        StringBuilder strReturnedAddress;
        try {
            List<Address> addresses = geocoder.getFromLocation(point.latitude, point.longitude, 1);

            if(addresses != null) {
                Address returnedAddress = addresses.get(0);
                strReturnedAddress = new StringBuilder();
                for(int i=0; i<returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
            }
            else{
                strReturnedAddress = new StringBuilder("No Address returned!");
            }
        } catch (IOException e) {
            e.printStackTrace();
            strReturnedAddress = new StringBuilder("Cannot get Address!");
        }
        return strReturnedAddress.toString();
    }

}
