package com.app.allryder.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.app.allryder.R;
import com.app.allryder.models.NavMenuItem;
import com.app.allryder.models.NavSectionItem;
import com.app.allryder.utils.NavigationDrawerInterface;

/**
 * Navigation Adapter class for sliding menu
 */
public class NavigationDrawerAdapter extends ArrayAdapter<NavigationDrawerInterface> {

    private LayoutInflater inflater;

    public NavigationDrawerAdapter(Context context, int textViewResourceId, NavigationDrawerInterface[] objects) {
        super(context, textViewResourceId, objects);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        NavigationDrawerInterface menuItem = this.getItem(position);
        if (menuItem.getType() == NavMenuItem.ITEM_TYPE) {
            view = getItemView(convertView, parent, menuItem);
        } else {
            view = getSectionView(convertView, parent, menuItem);
        }
        return view;
    }

    public View getItemView(View convertView, ViewGroup parentView, NavigationDrawerInterface navDrawerItem) {

        NavMenuItem menuItem = (NavMenuItem) navDrawerItem;
        NavItemHolder navMenuItemHolder = null;

        if (convertView == null) {
            navMenuItemHolder = new NavItemHolder();
            convertView = inflater.inflate(R.layout.row_navdrawer_menu_item, parentView, false);
            navMenuItemHolder.labelView = (TextView) convertView
                    .findViewById(R.id.tvNavMenuLabel);
            convertView.setTag(navMenuItemHolder);
        }

        if (navMenuItemHolder == null) {
            navMenuItemHolder = (NavItemHolder) convertView.getTag();
        }

        navMenuItemHolder.labelView.setText(menuItem.getIcon()+"  "+menuItem.getLabel());
        return convertView;
    }

    public View getSectionView(View convertView, ViewGroup parentView,
                               NavigationDrawerInterface navDrawerItem) {

        NavigationDrawerInterface menuSection = (NavSectionItem) navDrawerItem;
        NavItemHolder navMenuItemHolder = null;

        if (convertView == null) {
            navMenuItemHolder = new NavItemHolder();
            convertView = inflater.inflate(R.layout.row_navdrawer_section_item, parentView, false);
            navMenuItemHolder.labelView = (TextView) convertView
                    .findViewById(R.id.tvNavSectionLabel);

            convertView.setTag(navMenuItemHolder);
        }

        if (navMenuItemHolder == null) {
            navMenuItemHolder = (NavItemHolder) convertView.getTag();
        }

        navMenuItemHolder.labelView.setText(menuSection.getLabel());

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return this.getItem(position).getType();
    }

    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }


    private class NavItemHolder {
        private TextView labelView;
    }
}
