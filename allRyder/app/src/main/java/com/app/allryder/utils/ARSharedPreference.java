package com.app.allryder.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Singleton Class - for Shared preferences
 */
public class ARSharedPreference {

	private static volatile ARSharedPreference instance = null;
	Context context;

	SharedPreferences prefs;
	Editor ed;

	private ARSharedPreference(Context con) {
		this.context = con;
		prefs = context.getSharedPreferences("ARPreferences", Context.MODE_PRIVATE);
		ed = prefs.edit();
	}

	public static ARSharedPreference getInstance(Context con) {
		if (instance == null) {
			synchronized (ARSharedPreference.class) {
				if (instance == null)
					instance = new ARSharedPreference(con);
			}
		}
		return instance;
	}

	public void putString(String key, String value) {
		ed.putString(key, value);
		ed.commit();
	}

	public void putInt(String key, int value) {
		ed.putInt(key, value);
		ed.commit();
	}

	public void putLong(String key, long value) {
		ed.putLong(key, value);
		ed.commit();
	}

	public void putBoolean(String key, boolean value) {
		ed.putBoolean(key, value);
		ed.commit();
	}

	public void putFloat(String key, float value) {
		ed.putFloat(key, value);
		ed.commit();
	}

    /**
     * @param key - String
     * @return value - String (default value =  "")
     */
	public String getString(String key) {
		return prefs.getString(key, "");
	}

    /**
     * @param key - String
     * @return value - int (default value =  0)
     */
	public int getInt(String key) {
		return prefs.getInt(key, 0);
	}

    /**
     * @param key - String
     * @return value - float (default value =  0)
     */
	public float getFloat(String key) {
		return prefs.getFloat(key, 0);
	}

    /**
     * @param key - String
     * @return value - boolean (default value =  false)
     */
	public boolean getBoolean(String key) {
		return prefs.getBoolean(key, false);
	}

    /**
     * @param key - String
     * @return value - long (default value =  0)
     */
	public long getLong(String key) {
		return prefs.getLong(key, 0);
	}

    /**
     * remove key/value from shared preferences
     * @param key - String
     */
    public void remove(String key){
        ed.remove(key);
        ed.commit();
    }

    /**
     * clear All shared Preferences
     */
    public void clearSharedPreference(){
        ed.clear();
        ed.commit();
    }
}
