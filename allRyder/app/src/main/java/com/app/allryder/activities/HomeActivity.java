package com.app.allryder.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.app.allryder.R;
import com.app.allryder.fragments.DirectionFragment;
import com.app.allryder.fragments.HomeAddressFragment;
import com.app.allryder.fragments.HomeFragment;
import com.app.allryder.fragments.RouteFragment;
import com.app.allryder.models.NavMenuItem;
import com.app.allryder.models.NavSectionItem;
import com.app.allryder.utils.C;
import com.app.allryder.utils.NavigationDrawerInterface;
import com.app.allryder.utils.OnFragmentInteractionListener;
import com.app.allryder.views.adapters.NavigationDrawerAdapter;
import com.google.android.gms.maps.model.LatLng;


public class HomeActivity extends ActionBarActivity implements OnFragmentInteractionListener {

    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    //private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    NavigationDrawerInterface[] menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        /*
        initialise views
         */
        initViews();
    }

    /**
     * initialises all the views
     */
    private void initViews() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle(getResources().getString(R.string.home_activity_title));
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.lvLeftDrawer);
        setNavigationDrawerAdapter();
    }

    private void setNavigationDrawerAdapter() {
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        menu = new NavigationDrawerInterface[]{
                NavSectionItem.create("Community"),
                NavMenuItem.create("Tell your friends!", getResources().getString(R.string.icon_message), false),
                NavMenuItem.create("Show your love", getResources().getString(R.string.icon_heart), true),
                NavMenuItem.create("Feedback? Ideas? Live Support!", getResources().getString(R.string.icon_question_mark), true),
                NavSectionItem.create("About Us"),
                NavMenuItem.create("Who is allRyder", getResources().getString(R.string.icon_info), false),
                NavMenuItem.create("Copyright,legal, etc", getResources().getString(R.string.icon_file), false)};

        mDrawerList.setAdapter(new NavigationDrawerAdapter(this, R.layout.row_navdrawer_menu_item, menu));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                //getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                //getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        performFragmentTransaction(HomeFragment.newInstance(), false);
//        LocationController locationController = LocationController.getInstance(this);
//        LatLng to = locationController.getCurrentLocation(this);
//        LatLng from = to;
//        performFragmentTransaction(RouteFragment.newInstance(from, to));
    }

    /**
     * @param fragment
     */
    private void performFragmentTransaction(Fragment fragment, boolean isBackStack) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.fragment_container, fragment);
        if(isBackStack)
            ft.addToBackStack(null);
        ft.commit();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch (item.getItemId()) {
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onFragmentInteraction(int id, LatLng from, LatLng to) {

        switch (id) {
            case C.SHOW_ROUTE_FRAGMENT:
                performFragmentTransaction(RouteFragment.newInstance(from, to), true);
                break;
            case C.SHOW_DIRECTION_FRAGMENT:
                performFragmentTransaction(DirectionFragment.newInstance(), true);
                break;
            case C.SHOW_HOME_ADDRESS_FRAGMENT:
                performFragmentTransaction(HomeAddressFragment.newInstance(), true);
                break;
        }
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public void selectItem(int position) {
        NavigationDrawerInterface selectedItem = menu[position];

        mDrawerList.setItemChecked(position, true);

        if (selectedItem.updateActionBarTitle()) {
            setTitle(selectedItem.getLabel());
        }

        if (this.mDrawerLayout.isDrawerOpen(this.mDrawerList)) {
            mDrawerLayout.closeDrawer(mDrawerList);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
        getSupportActionBar().setTitle(mTitle);
    }
}
