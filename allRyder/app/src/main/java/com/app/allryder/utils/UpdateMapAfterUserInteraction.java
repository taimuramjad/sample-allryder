package com.app.allryder.utils;

/**
 * Map Activity must implement this interface
 */
public interface UpdateMapAfterUserInteraction {

    public void onUpdateMapAfterUserInteraction();
    public void onUpdateMapOnUserInteraction();
    public void setMapMoving(boolean moving);

}
