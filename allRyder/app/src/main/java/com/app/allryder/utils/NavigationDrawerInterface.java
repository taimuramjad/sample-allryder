package com.app.allryder.utils;

/**
 * Created by hamid on 5/7/15.
 */
public interface NavigationDrawerInterface  {
    public String getLabel();
    public int getType();
    public boolean isEnabled();
    public boolean updateActionBarTitle();
}
