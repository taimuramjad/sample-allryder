package com.app.allryder.views.Widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.app.allryder.utils.FontCache;

/**
 * Custom Text View Widget for FontAwesomeFonts
 */
public class FontAwesomeTextView extends TextView
{

    public FontAwesomeTextView(Context context)
    {
        super(context);
    }

    public FontAwesomeTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    public FontAwesomeTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
    }

    @Override
    public void setTypeface(Typeface tf, int style)
    {
        if (isInEditMode()) return;

        Context ctx = getContext();
        super.setTypeface(FontCache.getFont(ctx, "fonts/fontawesome-webfont.ttf"));
    }
}
