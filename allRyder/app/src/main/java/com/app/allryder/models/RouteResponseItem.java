package com.app.allryder.models;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by hamid on 5/10/15.
 */
public class RouteResponseItem {
    public ArrayList<RouteItem> routes;
    public HashMap<String,  HashMap<String, String>> provider_attributes;
}
