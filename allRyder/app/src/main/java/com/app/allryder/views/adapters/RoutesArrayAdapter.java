package com.app.allryder.views.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.app.allryder.R;
import com.app.allryder.models.RouteItem;

import java.util.ArrayList;

/**
 * Navigation Adapter class for sliding menu
 */
public class RoutesArrayAdapter extends BaseAdapter {

    private LayoutInflater inflater;
    private Context mContext;
    private ArrayList<RouteItem> mRoutesData;

    public RoutesArrayAdapter(Context context, ArrayList<RouteItem> data) {
        mContext = context;
        this.inflater = LayoutInflater.from(context);
        mRoutesData = data;

    }

    @Override
    public int getCount() {
        if(mRoutesData == null)
            return  0;
        return mRoutesData.size();
    }

    @Override
    public RouteItem getItem(int i) {
        return mRoutesData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RouteItem item = getItem(position);

        RoutesItemHolder viewHolder = null;

        if (convertView == null) {
            viewHolder = new RoutesItemHolder();
            convertView = inflater.inflate(R.layout.row_routes_item, parent, false);
            viewHolder.tv1 = (TextView) convertView
                    .findViewById(R.id.tv1);
            viewHolder.tv2 = (TextView) convertView
                    .findViewById(R.id.tv2);
            viewHolder.tv3 = (TextView) convertView
                    .findViewById(R.id.tv3);
            convertView.setTag(viewHolder);
        }

        if (viewHolder == null) {
            viewHolder = (RoutesItemHolder) convertView.getTag();
        }

        viewHolder.tv1.setText(item.type);
        viewHolder.tv2.setText(item.provider);
        viewHolder.tv3.setText(item.segments.get(0).description);

        return convertView;
    }

    private class RoutesItemHolder {
        private TextView tv1;
        private TextView tv2;
        private TextView tv3;
    }
}
