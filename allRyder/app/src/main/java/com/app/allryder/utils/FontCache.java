package com.app.allryder.utils;

import android.content.Context;
import android.graphics.Typeface;

import java.util.HashMap;
import java.util.Map;

/**
 * This class will cache the custom fonts for reusability.
 */
public class FontCache
{
    private static Map<String, Typeface> fontMap = new HashMap<String, Typeface>();

    public static Typeface getFont(final Context context, final String fontName)
    {

        if (fontMap.containsKey(fontName)) {
            /*
            return font from the map if already used
            */
            return fontMap.get(fontName);
        } else {
            /*
            if using first time then create type face from assets
            */
            Typeface tf = Typeface.createFromAsset(context.getAssets(), fontName);
            fontMap.put(fontName, tf);
            return tf;
        }
    }
}
