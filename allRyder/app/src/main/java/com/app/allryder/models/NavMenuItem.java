package com.app.allryder.models;

import com.app.allryder.utils.NavigationDrawerInterface;

/**
 * Created by hamid on 5/7/15.
 */
public class NavMenuItem implements NavigationDrawerInterface {

    public static final int ITEM_TYPE = 1 ;

    private String label ;
    private String icon ;
    private boolean updateActionBarTitle ;

    private NavMenuItem() {
    }

    public static NavMenuItem create(String label, String icon, boolean updateActionBarTitle) {
        NavMenuItem item = new NavMenuItem();
        item.setLabel(label);
        item.setIcon(icon);
        item.setUpdateActionBarTitle(updateActionBarTitle);
        return item;
    }

    @Override
    public int getType() {
        return ITEM_TYPE;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean updateActionBarTitle() {
        return this.updateActionBarTitle;
    }

    public void setUpdateActionBarTitle(boolean updateActionBarTitle) {
        this.updateActionBarTitle = updateActionBarTitle;
    }
}