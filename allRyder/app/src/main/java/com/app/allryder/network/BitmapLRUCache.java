package com.app.allryder.network;

import android.graphics.Bitmap;
import android.util.LruCache;

import com.android.volley.toolbox.ImageLoader.ImageCache;

/**
 * This is
 * Created by fn on 2014-05-24.
 */
public final class BitmapLRUCache extends LruCache<String, Bitmap> implements ImageCache
{
    public BitmapLRUCache(int maxSize)
    {
        super(maxSize);
    }

    @Override
    protected int sizeOf(final String key, final Bitmap value)
    {
        return value.getRowBytes() * value.getHeight();
    }

    @Override
    public Bitmap getBitmap(final String url)
    {
        return get(url);
    }

    @Override
    public void putBitmap(final String url, final Bitmap bitmap)
    {
        put(url, bitmap);
    }
}
