package com.app.allryder.activities;

import android.app.Application;

import com.app.allryder.network.ARVolley;
import com.app.allryder.utils.ARHelper;

/**
 * extends Application class.
 * initiates some singleton classes and override some Application methods.
 */
public class ARApplication extends Application
{
    @Override
    public void onCreate()
    {

        ARHelper.getInstance();
        ARVolley.init(this);
    }

}
