package com.app.allryder.utils;

/**
 * Singleton class; Contains Constant Keys
 */
public class ARHelper
{
    static ARHelper instance;

    /**
     * Default Constructor is private; since its a singleton class
     */
    private ARHelper(){
    }

    /**
     * get singleton instance of DKHelper
     * @return DKHelper
     */
    public static ARHelper getInstance()
    {
        if (instance == null){
            synchronized (ARHelper.class) {
                if (instance == null) {
                    instance = new ARHelper();
                }
            }
        }
        return instance;
    }
}
