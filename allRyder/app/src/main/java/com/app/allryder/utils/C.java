package com.app.allryder.utils;

/**
 * this is class will contain public static (final) constant String/integers etc
 */
public class C {

    public static final int SHOW_ROUTE_FRAGMENT = 1;
    public static final int SHOW_DIRECTION_FRAGMENT = 2;
    public static final int SHOW_HOME_ADDRESS_FRAGMENT = 3;
    public static final String NEVER_SHOW_AGAIN = "never_show_again";
}