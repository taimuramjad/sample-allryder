package com.app.allryder.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.app.allryder.R;
import com.app.allryder.models.RouteResponseItem;
import com.app.allryder.network.ARNetworkHelper;
import com.app.allryder.utils.OnFragmentInteractionListener;
import com.app.allryder.views.adapters.RoutesArrayAdapter;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.app.allryder.utils.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.app.allryder.fragments.RouteFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class RouteFragment extends Fragment implements View.OnClickListener {


    private static final String BUNDLEKEY_FROM = "from";
    private static final String BUNDLEKEY_To = "to";
    private OnFragmentInteractionListener mListener;
    private Context mContext;

    private ListView mListView;
    private RoutesArrayAdapter mAdapter;


    /**
     * Use this method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static RouteFragment newInstance(LatLng from, LatLng to) {
        RouteFragment fragment = new RouteFragment();
        Bundle args = new Bundle();
        args.putParcelable(BUNDLEKEY_FROM, from);
        args.putParcelable(BUNDLEKEY_To, to);
        fragment.setArguments(args);
        return fragment;
    }
    public RouteFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_route, container, false);
        mContext = inflater.getContext();

        initViews(view);

        Bundle args = getArguments();
        if (args != null) {
            LatLng from = args.getParcelable(BUNDLEKEY_FROM);
            LatLng to = args.getParcelable(BUNDLEKEY_To);
            getRouteData(from, to);
        }

        return view;
    }

    /**
     * initializes all views
     * @param view
     */
    private void initViews(View view) {

        mListView = (ListView) view.findViewById(R.id.lvRoutes);
        TextView tvEmpty = (TextView) view.findViewById(R.id.tvEmpty);
        mListView.setEmptyView(tvEmpty);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }




    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
        }
    }

    private void getRouteData(LatLng from, LatLng to) {
        String response = ARNetworkHelper.getTravelResponse(mContext);
        if(response == null){
            return;
        }

        Gson mGson = new Gson();
        RouteResponseItem routeResponse = mGson.fromJson(response, RouteResponseItem.class);

        mAdapter = new RoutesArrayAdapter(mContext, routeResponse.routes);
        mListView.setAdapter(mAdapter);


    }
}
