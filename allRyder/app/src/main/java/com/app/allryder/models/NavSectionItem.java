package com.app.allryder.models;

import com.app.allryder.utils.NavigationDrawerInterface;

/**
 * Created by hamid on 5/7/15.
 */
public class NavSectionItem implements NavigationDrawerInterface {

        public static final int SECTION_TYPE = 0;
        private String label;

        private NavSectionItem() {
        }

        public static NavSectionItem create(String label ) {
            NavSectionItem section = new NavSectionItem();
            section.setLabel(label);
            return section;
        }

        @Override
        public int getType() {
            return SECTION_TYPE;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        @Override
        public boolean isEnabled() {
            return false;
        }

        @Override
        public boolean updateActionBarTitle() {
            return false;
        }

}
