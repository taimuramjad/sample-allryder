package com.app.allryder.fragments;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.app.allryder.R;
import com.app.allryder.utils.ARSharedPreference;
import com.app.allryder.utils.C;
import com.app.allryder.utils.LocationController;
import com.app.allryder.utils.OnFragmentInteractionListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.app.allryder.utils.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DirectionFragment#newInstance} factory method to
 * create an instance of this fragment.
 * Also implement {@link com.app.allryder.utils.UpdateMapAfterUserInteraction} interface for Map on touch listener call back
 */
public class DirectionFragment extends Fragment implements View.OnClickListener,
        GoogleMap.OnMapClickListener {

    private OnFragmentInteractionListener mListener;
    private GoogleMap mMap;
    private Marker mMarkerTo;
    private Marker mMarkerFrom;
    private Polyline mPolyline;
    PolylineOptions polyLineOptions;
    private Context mContext;

    private Button btnGo;
    private TextView tvBtnSwap;
    private TextView tvBtnTime;
    private EditText etFrom;
    private EditText etTo;


    /**
     * Use this method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static DirectionFragment newInstance() {
        DirectionFragment fragment = new DirectionFragment();
        return fragment;
    }

    public DirectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_direction, container, false);
        mContext = inflater.getContext();
        showMapInfoDialog();
        initViews(view);



        return view;
    }

    /**
     * initializes all views
     *
     * @param view
     */
    private void initViews(View view) {

        btnGo = (Button) view.findViewById(R.id.btnGo);
        tvBtnSwap = (TextView) view.findViewById(R.id.tvBtnSwap);
        tvBtnTime = (TextView) view.findViewById(R.id.tvBtnTime);
        etFrom = (EditText) view.findViewById(R.id.etFrom);
        etTo = (EditText) view.findViewById(R.id.etTo);

        btnGo.setOnClickListener(this);
        tvBtnSwap.setOnClickListener(this);
        tvBtnTime.setOnClickListener(this);

        etFrom.setText("Getting current location...");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap != null) {
            return;
        }
        mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        if (mMap != null) {
            startMap();
        }
    }

    private void startMap() {

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Gets to GoogleMap from the MapView and does initialization stuff
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);
        /*
         get current location of user and Updates the location and zoom of the MapView
          */
        LocationController locController = LocationController.getInstance(mContext);
        LatLng myCurrentLoc = locController.getCurrentLocation(mContext);
        /*
        animate camera from zoom 4 to zoom 16
         */
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                myCurrentLoc, 4);
        mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                myCurrentLoc, 16));

        /*
        marker for the location user wants to travel From
         */
        mMarkerFrom = mMap.addMarker(new MarkerOptions().position(
                myCurrentLoc
        ).draggable(false));

        /*
        set on camera change listener for center marker
         */
        //mMap.setOnCameraChangeListener(this);
        mMap.setOnMapClickListener(this);
        etFrom.setText("My Current Location");
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvBtnSwap:
                swap();
                break;
            case R.id.btnGo:
                if (mMarkerFrom == null || mMarkerTo == null) {
                    Toast.makeText(mContext, "select destination location first.", Toast.LENGTH_SHORT).show();
                    return;
                }
                mListener.onFragmentInteraction(C.SHOW_ROUTE_FRAGMENT, mMarkerFrom.getPosition(), mMarkerTo.getPosition());
                break;
            case R.id.tvBtnTime:
                break;
        }
    }

    /**
     * swap user travelling to and from locations and animate to the destination position
     */
    private void swap() {

        if (mMarkerFrom == null || mMarkerTo == null) {
            Toast.makeText(mContext, "select destination location first.", Toast.LENGTH_SHORT).show();
            return;
        }
        Marker tempMark = mMarkerFrom;
        mMarkerFrom = mMarkerTo;
        mMarkerTo = tempMark;

        String tempDestination = etTo.getText().toString();
        etTo.setText(etFrom.getText().toString());
        etFrom.setText(tempDestination);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                mMarkerTo.getPosition(), 16);
        mMap.animateCamera(cameraUpdate, 2000, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {

            }

            @Override
            public void onCancel() {

            }
        });

    }

    /**
     * on map click listener
     *
     * @param latLng
     */
    @Override
    public void onMapClick(LatLng latLng) {

        if (mPolyline != null) {
            mPolyline.remove();
        }

        LatLng mapCenter = latLng;//mMap.getCameraPosition().target;
        LocationController locationController = LocationController.getInstance(mContext);

        etTo.setText(locationController.getAddressFromLocation(mContext, mapCenter));

        if (mMarkerTo == null) {
            mMarkerTo = mMap.addMarker(new MarkerOptions().position(mapCenter).draggable(false));
        } else {
            mMarkerTo.setPosition(mapCenter);
        }
        polyLineOptions = new PolylineOptions().width(5).color(getResources().getColor(R.color.purple_dark)).geodesic(true);
        polyLineOptions.add(mMarkerFrom.getPosition(), mMarkerTo.getPosition());
        mPolyline = mMap.addPolyline(polyLineOptions);
    }

    private void showMapInfoDialog() {
        final ARSharedPreference shrdPref = ARSharedPreference.getInstance(mContext);
        if(shrdPref.getBoolean(C.NEVER_SHOW_AGAIN))
            return;
        new AlertDialog.Builder(mContext)
                .setMessage("Click on Map to select destination location.")
                .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Never show again", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        shrdPref.putBoolean(C.NEVER_SHOW_AGAIN, true);
                        dialog.dismiss();
                    }
                })
                .show();
    }

}
