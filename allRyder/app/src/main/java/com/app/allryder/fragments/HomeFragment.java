package com.app.allryder.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.allryder.R;
import com.app.allryder.utils.C;
import com.app.allryder.utils.LocationController;
import com.app.allryder.utils.OnFragmentInteractionListener;
import com.app.allryder.utils.UpdateMapAfterUserInteraction;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 * Also implement {@link UpdateMapAfterUserInteraction} interface for Map on touch listener call back
 *
 */
public class HomeFragment extends Fragment implements View.OnClickListener{

    private OnFragmentInteractionListener mListener;
    private GoogleMap mMap;
    private Context mContext;


    private TextView tvBtnWork;
    private TextView tvBtnHome;
    private TextView tvBtnDirections;
    private MapView mMapView;

    /**
     * Use this method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment HomeFragment.
     */
    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        mContext = inflater.getContext();
        initViews(view);

        return view;
    }

    /**
     * initializes all views
     * @param view
     */
    private void initViews(View view) {

//        mMapView = (MapView) view.findViewById(R.id.map);

        tvBtnWork = (TextView)view.findViewById(R.id.tvBtnWork);
        tvBtnHome = (TextView)view.findViewById(R.id.tvBtnHome);
        tvBtnDirections = (TextView)view.findViewById(R.id.tvBtnDirection);

        tvBtnHome.setOnClickListener(this);
        tvBtnWork.setOnClickListener(this);
        tvBtnDirections.setOnClickListener(this);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded(){
        if (mMap != null) {
            return;
        }
        mMap = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        //mMap = mMapView.getMap();
        if (mMap != null) {
            startMap();
        }
    }

    private void startMap() {

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(mContext);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Gets to GoogleMap from the MapView and does initialization stuff
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.setMyLocationEnabled(true);
        /*
         get current location of user and Updates the location and zoom of the MapView
          */
        LocationController locController = LocationController.getInstance(mContext);
        LatLng myCurrentLoc = locController.getCurrentLocation(mContext);
        /*
        animate camera from zoom 4 to zoom 16
         */
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                myCurrentLoc, 4);
        mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                myCurrentLoc, 16));
//
//        /*
//        marker for the location user wants to travel From
//         */
//        mMarkerCurrent = mMap.addMarker(new MarkerOptions().position(
//                myCurrentLoc
//        ).draggable(false));
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.tvBtnHome:
                mListener.onFragmentInteraction(C.SHOW_HOME_ADDRESS_FRAGMENT, null, null);
                //Toast.makeText(view.getContext(), "ToDo", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvBtnWork:
                Toast.makeText(view.getContext(), "ToDo", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tvBtnDirection:
                mListener.onFragmentInteraction(C.SHOW_DIRECTION_FRAGMENT, null, null);
                break;
        }
    }

}
