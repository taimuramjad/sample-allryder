package com.app.allryder.network;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

/**
 *
 */
public class ARVolley
{
    private static final int MAX_IMAGE_CACHE_ENTIRES = 6144000;

    static RequestQueue mRequestQueue;
    static ImageLoader mImageLoader;

    public static void init(final Context context)
    {
        mRequestQueue = Volley.newRequestQueue(context);
        mImageLoader = new ImageLoader(mRequestQueue, new BitmapLRUCache(MAX_IMAGE_CACHE_ENTIRES));
        VolleyLog.setTag("allryder");
    }

    /**
     * Returns instance of ImageLoader initialized with {@see FakeImageCache} which effectively means
     * that no memory caching is used. This is useful for images that you know that will be show
     * only once.
     */
    public static ImageLoader getImageLoader()
    {
        if (mImageLoader != null) {
            return mImageLoader;
        } else {
            throw new IllegalStateException("ImageLoader not initialized");
        }
    }
}
